#!/bin/bash

set -x

EMERGE_OPTS=(
    --verbose --quiet --update --noreplace
)

emerge ${EMERGE_OPTS[@]} dev-vcs/git eselect-repository

if ! eselect repository enable guru | grep -q 'already enabled'; then
    emerge --sync guru
fi

emerge ${EMERGE_OPTS[@]} dev-db/postgresql
# TODO: fix version
if [ -z "$(ls -A /var/lib/postgresql/14/data)" ]; then
    emerge --config dev-db/postgresql:14
fi
if [ "$(cat /proc/1/comm)" = "systemd" ]; then
    systemctl enable --now postgresql-14
else
    rc-update add postgresql-14
    rc-service postgresql-14 start
fi

# TODO: minio asyncio docker
TINDERBOX_DEPENDS=(
    dev-python/GitPython
    dev-python/psycopg
    dev-python/pygit2
    dev-python/requests
    dev-python/sqlalchemy-migrate
    dev-python/txrequests
    dev-util/buildbot
    dev-util/buildbot-badges
    dev-util/buildbot-console-view
    dev-util/buildbot-grid-view
    dev-util/buildbot-waterfall-view
    dev-util/buildbot-wsgi-dashboards
    dev-util/buildbot-www
)

emerge ${EMERGE_OPTS[@]} ${TINDERBOX_DEPENDS[@]}
